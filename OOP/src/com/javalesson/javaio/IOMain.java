package com.javalesson.javaio;

import com.javalesson.collections.map.treemap.AverageStudentGrade;
import com.javalesson.collections.map.treemap.SubjectGrade;
import com.javalesson.collections.map.treemap.TreeMapRunner;

import java.io.*;
import java.util.*;

public class IOMain {

    private static final String FILE_NAME = "GradeBook.txt";
    private static final String BINARY_FILE = "Students.bin";
    private static final String BUFFERED_FILE = "Buffered.txt";

    public static void main(String[] args) throws IOException {
        SortedMap<AverageStudentGrade, Set<SubjectGrade>> grades = TreeMapRunner.createGrades();

        Reader reader = new Reader();
        Writer writer = new Writer();

//        reader.nioReadFileWithBuffer(FILE_NAME);
//        writer.nioWriteWithBuffer(BUFFERED_FILE);

//        reader.nioReadWithStream(FILE_NAME);
//        writer.nioWriteWithStream(BUFFERED_FILE);

//        reader.readFileFull(FILE_NAME);
//        reader.readFile(FILE_NAME);
//        reader.nioReadWithChannel(FILE_NAME);
//
        writer.nioWriteWithChannel(BUFFERED_FILE);
//        Writer writer = new Writer();
//        writer.writeFile(grades, FILE_NAME);
//        processGrades(grades, writer, BINARY_FILE);
//        outputObjects(reader, BINARY_FILE);
//        writer.writeWithFormatter();

//        try(FileInputStream reader = new FileInputStream(FILE_NAME);
//            FileOutputStream writer = new FileOutputStream("GradeBookByte.txt")){
//
//            int c;
//            while((c = reader.read()) != -1) {
//                System.out.println(c);
//                writer.write(c);
//            }
//        }
//        FileUtils utils = new FileUtils();
//        utils.printNioFileDetails(FILE_NAME);


    }

    private static void processGrades(SortedMap<AverageStudentGrade, Set<SubjectGrade>> grades, Writer writer, String fileName) {
        List<Student> students = new ArrayList<>();
        for (AverageStudentGrade gradeKey : grades.keySet()) {
            students.add(new Student(gradeKey.getName(), gradeKey.getAverageGrade(), grades.get(gradeKey)));
        }

        writer.writeObject(students, fileName);
    }

    private static void outputObjects(Reader reader, String fileName) {
        List<Student> students = reader.readObject(fileName);
        for (Student student : students) {
            System.out.printf("%s, %.2f %n", student.getName(), student.getAverageGrade());
//            System.out.println(student.getGrades());
        }
    }


}
