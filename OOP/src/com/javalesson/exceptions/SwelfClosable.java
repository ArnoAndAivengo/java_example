package com.javalesson.exceptions;

import java.io.Closeable;
import java.io.IOException;

public interface SwelfClosable extends Closeable {

    @Override
    void close() throws IOException;
}
