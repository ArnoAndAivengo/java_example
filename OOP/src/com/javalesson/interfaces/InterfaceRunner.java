package com.javalesson.interfaces;

public class InterfaceRunner {

    public static void main(String[] args) {
        Priceble pizza = new Pizza("Dodo", 1, 20, Size.L);
        Priceble phone = new CellPhone("Nokia", "Xt1234", 1, 278);
        Priceble fridge = new Fridge("Samsung", "Locator", 1, 3);

        printDeliveryPrice(pizza);
        printDeliveryPrice(phone);
        printDeliveryPrice(fridge);
    }

    private static void printDeliveryPrice(Priceble del) {
        System.out.println("Delivery prise " + del.calcDeliveryPrice());
        System.out.println("OrderPrice prise " + del.calcOrderPrice());
    }
}
