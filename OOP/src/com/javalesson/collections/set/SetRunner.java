package com.javalesson.collections.set;

import java.util.*;

public class SetRunner {
    public static void main(String[] args) {

        Set<Car> sixCars = new HashSet<>();
        sixCars.add(new Car( "VW", "Golf",  35));
        sixCars.add(new Car( "Toyota", "auria", 40));

        Set<Car> europeCars = new HashSet<>();
        europeCars.add(new Car( "Toyota", "auria", 40));
        europeCars.add(new Car( "Toyota", "auria", 40));
        europeCars.add(new Car( "Toyota", "auria", 22));

        NavigableSet<Car> uniqueCars = new TreeSet<>(sixCars);
        uniqueCars.addAll(europeCars);
//        print(uniqueCars);

        // удаляет одинаковые
//        sixCars.removeAll(europeCars);
//        print(sixCars);

        // находит одинаковые в двух set
//        sixCars.retainAll(europeCars);

        // выводит разность
//        uniqueCars.removeAll(sixCars);

        SortedSet<Car> cars = uniqueCars.subSet(new Car("Toyota", "Auris", 40), new Car("Audi", "A3", 60));
        print(cars);

    }

    private static void print(Set<Car> cars) {
        System.out.printf("%-20s %-20s %-20s \n", "Car brand", "Model", "Price per day");
        for (Car car: cars) {
            System.out.printf("%-20s %-20s %-20s \n", car.getCarBrand(), car.getModel(), car.getPricePerDay());
        }
    }
}