package com.javalesson.collections.arrays;

import java.util.Arrays;

// Нахождение минимального числа
public class GradeBook {
    public static void main(String[] args) {
        int[][] gradesArray = {
            {97, 96, 70},
            {197, 196, 170},
            {297, 296, 270},
            {397, 396, 370},
            {497, 496, 470},
            {597, 596, 570},
            {697, 696, 670}
        };

        System.out.println("Minimal grade " + calcMin(gradesArray));

        varArgs();
        processArrays();
    }

    private static int calcMin(int[][] grades){
        int min = 1000;
        for(int[] row : grades) {
            for(int i: row) {
                if(min > i) {
                    min = i;
                }
            }
        }
        return min;
    }

    private static void varArgs() {
        double a = 0.56;
        double b = 1.92;
        double c = 3.45;
        double d = 5.01;

        System.out.println("Average of 2 elements is " + calcAverage(a,b));
        System.out.println("Average of 3 elements is " + calcAverage(a,b,c));
        System.out.println("Average of 4 elements is " + calcAverage(a,b,c,d));
    }

    private static double calcAverage(double... args) {
        double sum = 0;
        for (double i : args) {
            sum = sum + i;
        }
        return sum/args.length;
    }

    private static void processArrays() {
        double[] doubleArr = {8.9,5.56,8.12,45.0,77.1};
        Arrays.sort(doubleArr);
        System.out.println(Arrays.toString(doubleArr));

        int[] filledArray = new int[7];
        Arrays.fill(filledArray, 7);
        System.out.println(Arrays.toString(filledArray));

        int[] intArray = {1,2,3,4,5,6,7};
        int[] arrayCopy = new int[intArray.length];

        System.arraycopy(intArray, 0,arrayCopy, 0,intArray.length);
        System.out.println(Arrays.toString(arrayCopy));
    }
}
