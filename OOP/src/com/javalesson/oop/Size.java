package com.javalesson.oop;

public enum Size {
    VERY_SMALL("XS"), BIG("L"), AVERAGE("M"), SMALL("S"), VERY_BIG("XL"), UNDEFINED("");

    Size(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    private String abbreviation;
}
