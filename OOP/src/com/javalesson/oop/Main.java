package com.javalesson.oop;

public class Main {
    public static void main(String[] args) {

        Dog dog = new Dog();
        dog.setName("Doom");
        dog.setBreed("Boxer");
        dog.setSize(Size.AVERAGE);
        dog.bite();

        Dog dog2 = new Dog();
        dog2.setName("Argon");
        dog2.setBreed("Labrador");
        dog2.setSize(Size.BIG);
        dog2.bite();

        Dog doberman = new Dog();
        doberman.setName("Reks");
        doberman.setBreed("Doberman");
        doberman.setSize(Size.SMALL);
        doberman.bite();


        Size s = Size.SMALL;
        Size s1 = Size.valueOf("BIG");
        System.out.println(s1);

        for(Size c : Size.values())
            System.out.println(c);

    }
}