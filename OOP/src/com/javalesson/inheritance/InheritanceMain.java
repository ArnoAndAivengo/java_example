package com.javalesson.inheritance;

public class InheritanceMain {
    public static void main(String[] args) {

        Engine truckEngine = new Engine(6.0, EngineType.DIESEL, 900);
        Engine busEngine = new Engine(3.5, EngineType.DIESEL, 150);
        Auto bus = new Bus("Mercedes","Splinter", busEngine, 30,75,12);
        Auto truck = new Truck("Volvo", "VNL400", truckEngine, 300, 500, 1000);
        Auto electricCar = new ElectricCar("Tesla", "Model 8", 4, 100500);
//        Auto auto = new Auto("WV", "Polo", busEngine);

        runCar(bus);
        runCar(truck);
        runCar(electricCar);
//        runCar(auto);
    }

    private static void runCar(Auto auto) {
        auto.start();
        auto.stop();
        auto.energize();
    }
}
