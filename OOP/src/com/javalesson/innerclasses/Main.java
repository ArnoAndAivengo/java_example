package com.javalesson.innerclasses;

public class Main {
    public static void main(String[] args) {
        CellPhone phone = new CellPhone("Motorola", "K1000");
        phone.turnOn();

        phone.call("1234567890");
    }
}
