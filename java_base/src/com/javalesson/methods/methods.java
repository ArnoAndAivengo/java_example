package com.javalesson.methods;

public class methods {
    public static void main(String[] args){
        printMessage("Alex");
        System.out.println("Rectangle square = " + calcRectangleSquare(5, 10));
        System.out.println("Square = " + calcSquare(5));
        System.out.println("Sum of square = " + (calcRectangleSquare(5, 10) + calcSquare(10)));


        String strl = "I like coffee";
        boolean i_like = strl.startsWith("I like");

        System.out.println(i_like);


        String strl2 = strl.substring(1,7);
        System.out.println(strl2);

    }

    static void printMessage(String name){
        System.out.println("Hello, my name is " + name + "!");
    }

    static int calcRectangleSquare(int x, int y){
        int square = x*y;
        return square;
    }

    static int calcSquare(int x) {
        return x*x;
    }
}
